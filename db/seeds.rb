# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
if Rails.env.development? || Rails.env.production?
  
  AdminUser.create(
    email: "admin@fog_maintain.com",
    password: "qwerty123",
    password_confirmation: "qwerty123"
  )

  ServiceInfra.create(
    [
      {
        "id"=>1, 
        "name"=>"Fire Alarm Infrastructure", 
        "priority"=>1
      }, 
      {
        "id"=>2, 
        "name"=>"Smart Meter Infrastructure", 
        "priority"=>5
      }
    ]
  )


  EndDeviceInfo.create(
    [
      {
        "id"=>1, 
        "name"=>"CO Sensor", 
        "description"=>"It reads the CO Level in PPM", 
        "model"=>"008809809", 
        "manufacturer"=>"Cyber Dragon" 
      }, 
      {
        "id"=>2, 
        "name"=>"CO2 Sensor", 
        "description"=>"It reads the CO2 Level in PPM", 
        "model"=>"0088099000", 
        "manufacturer"=>"Cyber Dragon" 
      }, 
      {
        "id"=>3, 
        "name"=>"MOX GS822 Voltage Sensor", 
        "description"=>"Gas Sensor for Fire Detection", 
        "model"=>"GS822", 
        "manufacturer"=>"Woldra" 
      }, 
      {
        "id"=>4, 
        "name"=>"Temperature Sensor", 
        "description"=>"To read Temerature in Celcius", 
        "model"=>"CEL8777897", 
        "manufacturer"=>"Cyber Dragon" 
      }, 
      {
        "id"=>5, 
        "name"=>"MOX TGS880 Sensor", 
        "description"=>"Gas Sensor for Fire Detection", 
        "model"=>"89789797798", 
        "manufacturer"=>"Woldra" 
      }, 
      {
        "id"=>6, 
        "name"=>"Ion Voltage Sensor", 
        "description"=>"Sensor to read Ion votage in Gas", 
        "model"=>"ION8088909", 
        "manufacturer"=>"Woldra"
      }, 
      {
        "id"=>7, 
        "name"=>"Smart Meter Sensor", 
        "description"=>"Device to Read Electric Consumption in kwh.", 
        "model"=>"SMT900889", 
        "manufacturer"=>"Cyber Dragon" 
      }, 
      {
        "id"=>8, 
        "name"=>"Smoke Voltage Sensor", 
        "description"=>"Reads Smoke Voltage in the Gas", 
        "model"=>"098977888798", 
        "manufacturer"=>"Woldra"
      }
    ]
  )


  InfraParameter.create(
    [
      {
        "id"=>2, 
        "name"=>"CO", 
        "unit"=>"ppm",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>3, 
        "name"=>"CO2", 
        "unit"=>"ppm",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>4, 
        "name"=>"MOX GS822", 
        "unit"=>"V",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>1, 
        "name"=>"Temperature", 
        "unit"=>"celcius",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>5, 
        "name"=>"MOX TGS880", 
        "unit"=>"V",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>6, 
        "name"=>"Smoke Voltage", 
        "unit"=>"V",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>7, 
        "name"=>"Ion Voltage", 
        "unit"=>"V",
        "retrieve_type"=>"average"
      }, 
      {
        "id"=>8, 
        "name"=>"Electric Consumption", 
        "unit"=>"kwh",
        "retrieve_type"=>"last_value"
      }
    ]
  )

  
  EndDeviceInfoInfraParameter.create(
    [
      {
        "id"=>1, 
        "end_device_info_id"=>1, 
        "infra_parameter_id"=>2
      }, 
      {
        "id"=>2, 
        "end_device_info_id"=>2, 
        "infra_parameter_id"=>3
      }, 
      {
        "id"=>3, 
        "end_device_info_id"=>3, 
        "infra_parameter_id"=>4
      }, 
      {
        "id"=>4, 
        "end_device_info_id"=>4, 
        "infra_parameter_id"=>1
      }, 
      {
        "id"=>5, 
        "end_device_info_id"=>5, 
        "infra_parameter_id"=>5
      }, 
      {
        "id"=>6, 
        "end_device_info_id"=>6, 
        "infra_parameter_id"=>7
      }, 
      {
        "id"=>7, 
        "end_device_info_id"=>7, 
        "infra_parameter_id"=>8
      }, 
      {
        "id"=>8, 
        "end_device_info_id"=>8, 
        "infra_parameter_id"=>6
      }
    ] 
  )

  icfn_node = IcfnNode.new
  icfn_node.id = 1
  icfn_node.name = "icfn_node1"
  icfn_node.base_url = "http://0.0.0.0:3050"
  icfn_node.authentication_token = "UhLYPNUCszwLsyR8HXsB"
  icfn_node.is_recently_updated = false

  icfn_node_location = icfn_node.build_location
  icfn_node_location.latitude, icfn_node_location.longitude = 10.7589509, 78.8110481

  icfn_node_security_template = icfn_node.build_security_template
  icfn_node_security_template.symmetric_encryption_scheme = "AES-128-CBC"
  
  icfn_node.save!


  icfn_node = IcfnNode.new
  icfn_node.id = 2
  icfn_node.name = "icfn_node2"
  icfn_node.base_url = "http://0.0.0.0:3050"
  icfn_node.authentication_token = "2GH5xnnZ5WsSH1xM7Kyx"
  icfn_node.is_recently_updated = false

  icfn_node_location = icfn_node.build_location
  icfn_node_location.latitude, icfn_node_location.longitude = 11.8934449, 75.6493328

  icfn_node_security_template = icfn_node.build_security_template
  icfn_node_security_template.symmetric_encryption_scheme = "AES-192-CBC"
  
  icfn_node.save!


  cloud_server = CloudServer.new
  cloud_server.id = 1
  cloud_server.name = "amazon aws ec2"
  cloud_server.base_url = "http://0.0.0.0:5000"
  cloud_server.authentication_token = "6csmP18H6Ts_yAzFzzzy"
  cloud_server.is_recently_updated = false

  cloud_server_security_template = cloud_server.build_security_template
  cloud_server_security_template.symmetric_encryption_scheme = "AES-128-CBC"

  cloud_server.save!


  house = House.new
  house.id = 2
  house.name = "Wolf House"
  house.address = "Celtic Ville, Wolfstreet"
  house.landmark = "Near Wolfenstein Castle"
  house.description = "2 km from Dragon Bus Terminal"
  house.authentication_token = "ET5Qh_mUDaPM-szAFmd2"
  house.is_recently_updated = false

  location = house.build_location
  location.latitude, location.longitude = 10.7606682, 78.8147379
  
  house_security_template = house.build_security_template
  house_security_template.symmetric_encryption_scheme = "AES-128-CBC"

  house.save!

  user = User.new
  user.id = 2
  user.email = "anjalitp@gmail.com"
  user.name = "Anjali T P"
  user.phone_number = "9995516145"
  user.house_id = 2
  user.save!


  house = House.new
  house.id = 1
  house.name = "Dragon House"
  house.address = "Dragon Ville, Dragon Street"
  house.landmark = "Near Pheonix House"
  house.description = "2 km from Dragon Bus Terminal"
  house.authentication_token = "LWeVfBsTSe8N1qum6Xm5"
  house.is_recently_updated = false

  location = house.build_location
  location.latitude, location.longitude = 10.7598267, 78.81599
  
  house_security_template = house.build_security_template
  house_security_template.symmetric_encryption_scheme = "AES-192-CBC"

  house.save!

  user = User.new
  user.id = 1
  user.email = "arjunthedragon@gmail.com"
  user.name = "Arjun K P"
  user.phone_number = "9072263675"
  user.house_id = 1
  user.save!


  HouseServiceInfra.create(
    [
      {
        "id" => 1,
        "house_id" => 1,
        "service_infra_id"=>1
      },
      {
        "id" => 2,
        "house_id" => 1,
        "service_infra_id"=>2
      },
      {
        "id" => 3,
        "house_id" => 2,
        "service_infra_id"=>1
      },
      {
        "id" => 4,
        "house_id" => 2,
        "service_infra_id"=>2
      }
    ]
  )


  DeviceGateway.create(
    [
      {
        "id"=>1, 
        "name"=>"Fire Alarm Gateway", 
        "base_url"=>"http://localhost:3000",
        "house_id"=>1,
        "channel_key"=>"v8Gx76BSUx2WqndKzSV_",
        "channel_encryption_scheme"=>DeviceGateway::DEFAULT_CHANNEL_ENCRYPTION_SCHEME
      }, 
      {
        "id"=>2, 
        "name"=>"Smart Meter Gateway", 
        "base_url"=>"http://localhost:3000",
        "house_id"=>1,
        "channel_key"=>"yZaG3WiCCg4RqkgcLCqh",
        "channel_encryption_scheme"=>DeviceGateway::DEFAULT_CHANNEL_ENCRYPTION_SCHEME
      },
      {
        "id"=>3, 
        "name"=>"Fire Alarm Gateway", 
        "base_url"=>"http://localhost:3000",
        "house_id"=>2,
        "channel_key"=>"xEzphaBzuymfC22h6NfH",
        "channel_encryption_scheme"=>DeviceGateway::DEFAULT_CHANNEL_ENCRYPTION_SCHEME
      }, 
      {
        "id"=>4, 
        "name"=>"Smart Meter Gateway", 
        "base_url"=>"http://localhost:3000",
        "house_id"=>2,
        "channel_key"=>"5mXxSv5pVoBkhjs2QmCe",
        "channel_encryption_scheme"=>DeviceGateway::DEFAULT_CHANNEL_ENCRYPTION_SCHEME
      }
    ]
  )


  HouseSection.create(
    [
      {
        :id=>1, 
        :name=>"Kitchen", 
        :house_id=>1
      }, 
      {
        :id=>2, 
        :name=>"Living Room", 
        :house_id=>1
      }, 
      {
        :id=>3, 
        :name=>"Bedroom", 
        :house_id=>1
      },
      {
        :id=>4, 
        :name=>"Kitchen", 
        :house_id=>2
      }, 
      {
        :id=>5, 
        :name=>"Living Room", 
        :house_id=>2
      }, 
      {
        :id=>6, 
        :name=>"Bedroom", 
        :house_id=>2
      }
    ]
  )


  EndDevice.create(
    [
      {
        "id"=>1, 
        "house_section_id"=>1, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>1, 
        "name"=>"CO Sensor Device Kitchen", 
        "should_alert_status"=>true, 
        "serial_number"=>"hhjhjkhjkhhj", 
        "device_gateway_id"=>1
      }, 
      {
        "id"=>2, 
        "house_section_id"=>1, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>2, 
        "name"=>"CO2 Sensor Device Kitchen", 
        "should_alert_status"=>true, 
        "serial_number"=>"kjgfghffd", 
        "device_gateway_id"=>1
      }, 
      {
        "id"=>3, 
        "house_section_id"=>1, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>3, 
        "name"=>"MOX GS822 Voltage Sensor Device  Kitchen", 
        "should_alert_status"=>true, 
        "serial_number"=>"wrwrwerwerwf", 
        "device_gateway_id"=>1
      }, 
      {
        "id"=>4, 
        "house_section_id"=>1, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>5, 
        "name"=>"MOX TGS880 Sensor Device  Kitchen", 
        "should_alert_status"=>true, 
        "serial_number"=>"9898009", 
        "device_gateway_id"=>1
      }, 
      {
        "id"=>5, 
        "house_section_id"=>1, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>4, 
        "name"=>"Temperature Sensor Device  Kitchen", 
        "should_alert_status"=>true, 
        "serial_number"=>"lkjjkhjfhgdfd", 
        "device_gateway_id"=>1
      }, 
      {
        "id"=>6, 
        "house_section_id"=>1, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>6, 
        "name"=>"Ion Voltage Sensor Device  Kitchen", 
        "should_alert_status"=>true, 
        "serial_number"=>"cxhffjfjghg", 
        "device_gateway_id"=>1
      }, 
      {
        "id"=>7, 
        "house_section_id"=>1, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>8, 
        "name"=>"Smoke Voltage Sensor Device  Kitchen", 
        "should_alert_status"=>true, 
        "serial_number"=>"qwrwrewewere", 
        "device_gateway_id"=>1
      }, 
      {
        "id"=>8, 
        "house_section_id"=>2, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>4, 
        "name"=>"Temperature Sensor Device  Living Room", 
        "should_alert_status"=>true, 
        "serial_number"=>"dss87766787786", 
        "device_gateway_id"=>1
      }, 
      {
        "id"=>9, 
        "house_section_id"=>2, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>2, 
        "name"=>"CO2 Sensor Device  Living Room", 
        "should_alert_status"=>true, 
        "serial_number"=>"khhgjgjghg", 
        "device_gateway_id"=>1
      }, 
      {
        "id"=>11, 
        "house_section_id"=>3, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>2, 
        "name"=>"CO2 Sensor Device  Bedroom", 
        "should_alert_status"=>true, 
        "serial_number"=>"jkhjkhjkhkjhjh", 
        "device_gateway_id"=>1
      }, 
      {
        "id"=>10, 
        "house_section_id"=>2, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>1, 
        "name"=>"CO Sensor Device Living Room", 
        "should_alert_status"=>true, 
        "serial_number"=>"jkhjhhkjhj", 
        "device_gateway_id"=>1
      }, 
      {
        "id"=>12, 
        "house_section_id"=>2, 
        "service_infra_id"=>2, 
        "end_device_info_id"=>7, 
        "name"=>"Smart Meter Sensor Device", 
        "should_alert_status"=>true, 
        "serial_number"=>"SMhgfhghgffff", 
        "device_gateway_id"=>2
      },
      {
        "id"=>13, 
        "house_section_id"=>4, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>1, 
        "name"=>"CO Sensor Device Kitchen", 
        "should_alert_status"=>true, 
        "serial_number"=>"hhjhjkhjkhhj2", 
        "device_gateway_id"=>3
      }, 
      {
        "id"=>14, 
        "house_section_id"=>4, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>2, 
        "name"=>"CO2 Sensor Device Kitchen", 
        "should_alert_status"=>true, 
        "serial_number"=>"kjgfghffd2", 
        "device_gateway_id"=>3
      }, 
      {
        "id"=>15, 
        "house_section_id"=>4, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>3, 
        "name"=>"MOX GS822 Voltage Sensor Device  Kitchen", 
        "should_alert_status"=>true, 
        "serial_number"=>"wrwrwerwerwf2", 
        "device_gateway_id"=>3
      }, 
      {
        "id"=>16, 
        "house_section_id"=>4, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>5, 
        "name"=>"MOX TGS880 Sensor Device  Kitchen", 
        "should_alert_status"=>true, 
        "serial_number"=>"98980092", 
        "device_gateway_id"=>3
      }, 
      {
        "id"=>17, 
        "house_section_id"=>4, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>4, 
        "name"=>"Temperature Sensor Device  Kitchen", 
        "should_alert_status"=>true, 
        "serial_number"=>"lkjjkhjfhgdfd2", 
        "device_gateway_id"=>3
      }, 
      {
        "id"=>18, 
        "house_section_id"=>4, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>6, 
        "name"=>"Ion Voltage Sensor Device  Kitchen", 
        "should_alert_status"=>true, 
        "serial_number"=>"cxhffjfjghg2", 
        "device_gateway_id"=>3
      }, 
      {
        "id"=>19, 
        "house_section_id"=>4, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>8, 
        "name"=>"Smoke Voltage Sensor Device  Kitchen", 
        "should_alert_status"=>true, 
        "serial_number"=>"qwrwrewewere2", 
        "device_gateway_id"=>3
      }, 
      {
        "id"=>20, 
        "house_section_id"=>5, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>4, 
        "name"=>"Temperature Sensor Device  Living Room", 
        "should_alert_status"=>true, 
        "serial_number"=>"dss877667877862", 
        "device_gateway_id"=>3
      }, 
      {
        "id"=>21, 
        "house_section_id"=>5, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>2, 
        "name"=>"CO2 Sensor Device  Living Room", 
        "should_alert_status"=>true, 
        "serial_number"=>"khhgjgjghg2", 
        "device_gateway_id"=>3
      }, 
      {
        "id"=>23, 
        "house_section_id"=>6, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>2, 
        "name"=>"CO2 Sensor Device  Bedroom", 
        "should_alert_status"=>true, 
        "serial_number"=>"jkhjkhjkhkjhjh2", 
        "device_gateway_id"=>3
      }, 
      {
        "id"=>22, 
        "house_section_id"=>5, 
        "service_infra_id"=>1, 
        "end_device_info_id"=>1, 
        "name"=>"CO Sensor Device Living Room", 
        "should_alert_status"=>true, 
        "serial_number"=>"jkhjhhkjhj2", 
        "device_gateway_id"=>3
      }, 
      {
        "id"=>24, 
        "house_section_id"=>5, 
        "service_infra_id"=>2, 
        "end_device_info_id"=>7, 
        "name"=>"Smart Meter Sensor Device", 
        "should_alert_status"=>true, 
        "serial_number"=>"SMhgfhghgffff2", 
        "device_gateway_id"=>4
      }
    ]
  )

  
  CommunicationChannel.create!(
    [
      {
        "id" => 1, 
        "communication_channel_type" => CommunicationChannel::COMMUNICATION_CHANNEL_TYPE_HOUSE_TO_ICFN,
        "transmitter_communication_channelable_id" => 1,
        "receiver_communication_channelable_id" => 1, 
        "channel_key" => "c2mC_zpvjcYphBCrEay2"
      }, 
      {
        "id" => 2, 
        "communication_channel_type" => CommunicationChannel::COMMUNICATION_CHANNEL_TYPE_HOUSE_TO_ICFN,
        "transmitter_communication_channelable_id" => 1,
        "receiver_communication_channelable_id" => 2, 
        "channel_key" => "w2xAPwwioWKQjRvXcE8K"
      }, 
      {
        "id" => 3, 
        "communication_channel_type" => CommunicationChannel::COMMUNICATION_CHANNEL_TYPE_HOUSE_TO_CLOUD,
        "transmitter_communication_channelable_id" => 1,
        "receiver_communication_channelable_id" => 1, 
        "channel_key" => "59JV1T89Tmwn1UM5KVv6"
      },
      {
        "id" => 4, 
        "communication_channel_type" => CommunicationChannel::COMMUNICATION_CHANNEL_TYPE_HOUSE_TO_ICFN,
        "transmitter_communication_channelable_id" => 2,
        "receiver_communication_channelable_id" => 1, 
        "channel_key" => "TyXJKxsNusxYyukA4csz"
      }, 
      {
        "id" => 5, 
        "communication_channel_type" => CommunicationChannel::COMMUNICATION_CHANNEL_TYPE_HOUSE_TO_ICFN,
        "transmitter_communication_channelable_id" => 2,
        "receiver_communication_channelable_id" => 2, 
        "channel_key" => "vq2uc7ba6kuAWxvGTZPp"
      }, 
      {
        "id" => 6, 
        "communication_channel_type" => CommunicationChannel::COMMUNICATION_CHANNEL_TYPE_HOUSE_TO_CLOUD,
        "transmitter_communication_channelable_id" => 2,
        "receiver_communication_channelable_id" => 1, 
        "channel_key" => "u4WLSEGRZpSSXRRMcSGQ"
      },
      {
        "id" => 7, 
        "communication_channel_type" => CommunicationChannel::COMMUNICATION_CHANNEL_TYPE_ICFN_TO_CLOUD,
        "transmitter_communication_channelable_id" => 1,
        "receiver_communication_channelable_id" => 1, 
        "channel_key" => "5t4GKxjv7y6PjSTx2BKZ"
      }, 
      {
        "id" => 8, 
        "communication_channel_type" => CommunicationChannel::COMMUNICATION_CHANNEL_TYPE_ICFN_TO_CLOUD,
        "transmitter_communication_channelable_id" => 2,
        "receiver_communication_channelable_id" => 1, 
        "channel_key" => "4NZmk9CqKxjszxF8TiH4"
      }
    ]
  )

  
  PlatformError.create(
    [
      {
        "id" => 1,
        "platform_errorable_type" => IcfnNode.to_s,
        "platform_errorable_id" => 1,
        "reported_at_timestamp" => Time.now.to_i,
        "platform_error_type" => PlatformError::PLATFORM_ERROR_TYPE_ICFN_FAILURE
      },
      {
        "id" => 2,
        "platform_errorable_type" => IcfnNode.to_s,
        "platform_errorable_id" => 1,
        "reported_at_timestamp" => Time.now.to_i,
        "platform_error_type" => PlatformError::PLATFORM_ERROR_TYPE_ICFN_FAILURE
      },
      {
        "id" => 3,
        "platform_errorable_type" => IcfnNode.to_s,
        "platform_errorable_id" => 2,
        "reported_at_timestamp" => Time.now.to_i,
        "platform_error_type" => PlatformError::PLATFORM_ERROR_TYPE_ICFN_FAILURE
      },
      {
        "id" => 4,
        "platform_errorable_type" => IcfnNode.to_s,
        "platform_errorable_id" => 2,
        "reported_at_timestamp" => Time.now.to_i,
        "platform_error_type" => PlatformError::PLATFORM_ERROR_TYPE_ICFN_FAILURE
      },
      {
        "id" => 5,
        "platform_errorable_type" => IcfnNode.to_s,
        "platform_errorable_id" => 2,
        "reported_at_timestamp" => Time.now.to_i,
        "platform_error_type" => PlatformError::PLATFORM_ERROR_TYPE_ICFN_FAILURE
      },
      {
        "id" => 6,
        "platform_errorable_type" => CloudServer.to_s,
        "platform_errorable_id" => 1,
        "reported_at_timestamp" => Time.now.to_i,
        "platform_error_type" => PlatformError::PLATFORM_ERROR_TYPE_CLOUD_FAILURE
      },
      {
        "id" => 7,
        "platform_errorable_type" => CloudServer.to_s,
        "platform_errorable_id" => 1,
        "reported_at_timestamp" => Time.now.to_i,
        "platform_error_type" => PlatformError::PLATFORM_ERROR_TYPE_CLOUD_FAILURE
      },
      {
        "id" => 8,
        "platform_errorable_type" => House.to_s,
        "platform_errorable_id" => 1,
        "reported_at_timestamp" => Time.now.to_i,
        "platform_error_type" => PlatformError::PLATFORM_ERROR_TYPE_FOG_FAILURE
      },
      {
        "id" => 9,
        "platform_errorable_type" => House.to_s,
        "platform_errorable_id" => 1,
        "reported_at_timestamp" => Time.now.to_i,
        "platform_error_type" => PlatformError::PLATFORM_ERROR_TYPE_FOG_FAILURE
      }
    ]
  )

end