class CreateServiceInfras < ActiveRecord::Migration[5.0]
  def change
    create_table :service_infras do |t|

      t.string :name, index: true, unique: true
      t.integer :priority, index: true

      t.timestamps
    end

  end
end
