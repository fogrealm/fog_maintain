class CreateCommunicationChannels < ActiveRecord::Migration[5.0]
  def change
    create_table :communication_channels do |t|
      t.string :name
      t.string :communication_channel_type
      t.string :channel_key, unique: true
      
      t.integer :transmitter_communication_channelable_id, index: { name: "index_tx_comm_channelable_id" }
      t.string :transmitter_communication_channelable_type, index: { name: "index_tx_comm_channelable_type" }

      t.integer :receiver_communication_channelable_id, index: { name: "index_rx_comm_channelable_id" }
      t.string :receiver_communication_channelable_type, index: { name: "index_rx_comm_channelable_type" }

      t.timestamps
    end

    add_index :communication_channels, [ :transmitter_communication_channelable_id, :transmitter_communication_channelable_type, :receiver_communication_channelable_id, :receiver_communication_channelable_type],  unique: true, name: "communication_channelable_tx_rx_unique"
  end
end
