class CreateLocations < ActiveRecord::Migration[5.0]
  def change
    create_table :locations do |t|
      t.string :locatable_type
      t.integer :locatable_id
      t.float :latitude, {:precision=>11, :scale=>6}
      t.float :longitude, {:precision=>11, :scale=>6}
      t.float :accuracy

      t.timestamps
    end
  end
end
