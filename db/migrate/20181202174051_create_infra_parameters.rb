class CreateInfraParameters < ActiveRecord::Migration[5.0]
  def change
    create_table :infra_parameters do |t|

      t.string :name, unique: true
      t.string :unit
      t.string :retrieve_type, index: true

      t.timestamps
    end

    add_index :infra_parameters, [ :name, :unit], unique: true
  end
end