class CreateHouses < ActiveRecord::Migration[5.0]
  def change
    create_table :houses do |t|
      t.string :name
      t.text :address
      t.string :landmark
      t.text :description

      t.string :authentication_token, unique: true
      t.boolean :is_recently_updated, default: false
      
      t.timestamps
    end
  end
end
