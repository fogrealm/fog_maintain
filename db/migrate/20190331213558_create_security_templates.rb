class CreateSecurityTemplates < ActiveRecord::Migration[5.0]
  def change
    create_table :security_templates do |t|
      t.string :name
      t.text :private_key
      t.text :public_key

      t.string :symmetric_encryption_scheme
      
      t.integer :security_templatable_id, index: true
      t.string :security_templatable_type, index: true

      t.timestamps
    end

    add_index :security_templates, [ :security_templatable_id, :security_templatable_type],  unique: true, name: "security_templatable_type_id_type_unique"
  end
end
