class AddUniqueIdentifierToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :unique_identifier, :string, unique: true
  end
end
