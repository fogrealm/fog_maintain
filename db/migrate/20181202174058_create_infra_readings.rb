class CreateInfraReadings < ActiveRecord::Migration[5.0]
  def change
    create_table :infra_readings do |t|
      t.references :infra_parameter, index: true
      t.references :end_device, index: true

      t.float :value

      t.timestamps
    end
  end
end
