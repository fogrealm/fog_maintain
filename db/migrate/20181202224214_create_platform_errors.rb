class CreatePlatformErrors < ActiveRecord::Migration[5.0]
  def change
    create_table :platform_errors do |t|
      t.string :name

      t.string :platform_error_type, index: true, default: PlatformError::PLATFORM_ERROR_TYPE_ICFN_FAILURE

      t.integer :platform_errorable_id, index: true
      t.string :platform_errorable_type, index: true

      t.datetime :reported_at

      t.timestamps
    end
  end
end
