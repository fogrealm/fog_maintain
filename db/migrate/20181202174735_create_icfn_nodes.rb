class CreateIcfnNodes < ActiveRecord::Migration[5.0]
  def change
    create_table :icfn_nodes do |t|
      t.string :name
      t.string :base_url
      t.boolean :running_status, default: false

      t.string :authentication_token, unique: true
      t.boolean :is_recently_updated, default: false

      t.timestamps
    end
  end
end
