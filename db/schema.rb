# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190404214419) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "attachments", force: :cascade do |t|
    t.string   "attachable_type"
    t.integer  "attachable_id"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["attachable_type", "attachable_id"], name: "index_attachments_on_attachable_type_and_attachable_id", using: :btree
  end

  create_table "cloud_servers", force: :cascade do |t|
    t.string   "name"
    t.string   "base_url"
    t.boolean  "running_status",       default: false
    t.string   "authentication_token"
    t.boolean  "is_recently_updated",  default: false
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "communication_channels", force: :cascade do |t|
    t.string   "name"
    t.string   "communication_channel_type"
    t.string   "channel_key"
    t.integer  "transmitter_communication_channelable_id"
    t.string   "transmitter_communication_channelable_type"
    t.integer  "receiver_communication_channelable_id"
    t.string   "receiver_communication_channelable_type"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.index ["receiver_communication_channelable_id"], name: "index_rx_comm_channelable_id", using: :btree
    t.index ["receiver_communication_channelable_type"], name: "index_rx_comm_channelable_type", using: :btree
    t.index ["transmitter_communication_channelable_id", "transmitter_communication_channelable_type", "receiver_communication_channelable_id", "receiver_communication_channelable_type"], name: "communication_channelable_tx_rx_unique", unique: true, using: :btree
    t.index ["transmitter_communication_channelable_id"], name: "index_tx_comm_channelable_id", using: :btree
    t.index ["transmitter_communication_channelable_type"], name: "index_tx_comm_channelable_type", using: :btree
  end

  create_table "device_gateways", force: :cascade do |t|
    t.integer  "house_id"
    t.string   "name"
    t.string   "device_gateway_url"
    t.integer  "device_gateway_id"
    t.string   "base_url"
    t.string   "channel_key"
    t.string   "channel_encryption_scheme"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["device_gateway_id"], name: "index_device_gateways_on_device_gateway_id", using: :btree
    t.index ["house_id"], name: "index_device_gateways_on_house_id", using: :btree
  end

  create_table "end_device_info_infra_parameters", force: :cascade do |t|
    t.integer  "end_device_info_id"
    t.integer  "infra_parameter_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["end_device_info_id"], name: "index_end_device_info_infra_parameters_on_end_device_info_id", using: :btree
    t.index ["infra_parameter_id"], name: "index_end_device_info_infra_parameters_on_infra_parameter_id", using: :btree
  end

  create_table "end_device_infos", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "model"
    t.string   "manufacturer"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "end_devices", force: :cascade do |t|
    t.integer  "house_section_id"
    t.integer  "service_infra_id"
    t.integer  "end_device_info_id"
    t.integer  "device_gateway_id"
    t.string   "name"
    t.boolean  "should_alert_status", default: true
    t.string   "serial_number"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.index ["device_gateway_id"], name: "index_end_devices_on_device_gateway_id", using: :btree
    t.index ["end_device_info_id"], name: "index_end_devices_on_end_device_info_id", using: :btree
    t.index ["house_section_id"], name: "index_end_devices_on_house_section_id", using: :btree
    t.index ["service_infra_id"], name: "index_end_devices_on_service_infra_id", using: :btree
  end

  create_table "house_events", force: :cascade do |t|
    t.integer  "house_section_id"
    t.string   "name"
    t.string   "house_event_type", default: "inform"
    t.integer  "event_level",      default: 1
    t.datetime "reported_at"
    t.text     "event_message"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["house_section_id"], name: "index_house_events_on_house_section_id", using: :btree
  end

  create_table "house_sections", force: :cascade do |t|
    t.integer  "house_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["house_id"], name: "index_house_sections_on_house_id", using: :btree
  end

  create_table "house_service_infras", force: :cascade do |t|
    t.integer  "house_id"
    t.integer  "service_infra_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["house_id"], name: "index_house_service_infras_on_house_id", using: :btree
    t.index ["service_infra_id"], name: "index_house_service_infras_on_service_infra_id", using: :btree
  end

  create_table "houses", force: :cascade do |t|
    t.string   "name"
    t.text     "address"
    t.string   "landmark"
    t.text     "description"
    t.string   "authentication_token"
    t.boolean  "is_recently_updated",  default: false
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "icfn_nodes", force: :cascade do |t|
    t.string   "name"
    t.string   "base_url"
    t.boolean  "running_status",       default: false
    t.string   "authentication_token"
    t.boolean  "is_recently_updated",  default: false
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "infra_parameters", force: :cascade do |t|
    t.string   "name"
    t.string   "unit"
    t.string   "retrieve_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["name", "unit"], name: "index_infra_parameters_on_name_and_unit", unique: true, using: :btree
    t.index ["retrieve_type"], name: "index_infra_parameters_on_retrieve_type", using: :btree
  end

  create_table "infra_readings", force: :cascade do |t|
    t.integer  "infra_parameter_id"
    t.integer  "end_device_id"
    t.float    "value"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["end_device_id"], name: "index_infra_readings_on_end_device_id", using: :btree
    t.index ["infra_parameter_id"], name: "index_infra_readings_on_infra_parameter_id", using: :btree
  end

  create_table "locations", force: :cascade do |t|
    t.string   "locatable_type"
    t.integer  "locatable_id"
    t.float    "latitude"
    t.float    "longitude"
    t.float    "accuracy"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "platform_configs", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "platform_errors", force: :cascade do |t|
    t.string   "name"
    t.string   "platform_error_type",     default: "icfn_failure"
    t.integer  "platform_errorable_id"
    t.string   "platform_errorable_type"
    t.datetime "reported_at"
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.index ["platform_error_type"], name: "index_platform_errors_on_platform_error_type", using: :btree
    t.index ["platform_errorable_id"], name: "index_platform_errors_on_platform_errorable_id", using: :btree
    t.index ["platform_errorable_type"], name: "index_platform_errors_on_platform_errorable_type", using: :btree
  end

  create_table "security_templates", force: :cascade do |t|
    t.string   "name"
    t.text     "private_key"
    t.text     "public_key"
    t.string   "symmetric_encryption_scheme"
    t.integer  "security_templatable_id"
    t.string   "security_templatable_type"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["security_templatable_id", "security_templatable_type"], name: "security_templatable_type_id_type_unique", unique: true, using: :btree
    t.index ["security_templatable_id"], name: "index_security_templates_on_security_templatable_id", using: :btree
    t.index ["security_templatable_type"], name: "index_security_templates_on_security_templatable_type", using: :btree
  end

  create_table "service_infras", force: :cascade do |t|
    t.string   "name"
    t.integer  "priority"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_service_infras_on_name", using: :btree
    t.index ["priority"], name: "index_service_infras_on_priority", using: :btree
  end

  create_table "service_tasks", force: :cascade do |t|
    t.integer  "service_infra_id"
    t.integer  "infra_reading_id"
    t.integer  "service_priority",  default: 1
    t.integer  "gateway_timestamp"
    t.integer  "retry_count"
    t.integer  "max_retry_count"
    t.boolean  "is_drop_allowed",   default: false
    t.string   "service_task_type", default: "others"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.index ["infra_reading_id"], name: "index_service_tasks_on_infra_reading_id", using: :btree
    t.index ["max_retry_count"], name: "index_service_tasks_on_max_retry_count", using: :btree
    t.index ["retry_count"], name: "index_service_tasks_on_retry_count", using: :btree
    t.index ["service_infra_id"], name: "index_service_tasks_on_service_infra_id", using: :btree
    t.index ["service_priority"], name: "index_service_tasks_on_service_priority", using: :btree
    t.index ["service_task_type"], name: "index_service_tasks_on_service_task_type", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.integer  "house_id"
    t.string   "email",             default: "", null: false
    t.string   "name",              default: "", null: false
    t.string   "phone_number",                   null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "unique_identifier"
    t.index ["house_id"], name: "index_users_on_house_id", using: :btree
  end

end
