Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  root to: 'admin/dashboard#index'

  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resource :static_pages, only: [] do
    collection do
      get :home
    end
  end

  resources :houses, only: [:show, :update, :index] do
    
    member do
      get :check_updates
      post :set_house_recently_updated
    end

    resources :house_sections do
    end
    
    resources :device_gateways, only: [:show, :index] do  
    end

    resources :end_devices, only: [:show, :index] do
    end

    resources :service_infras, only: [:show, :index] do
    end

  end


  resources :infra_parameters, only: [:show, :index] do
  end

  resources :end_device_infos, only: [:show, :index] do
  end

  resources :end_device_info_infra_parameters, only: [:show, :index] do
  end

  resources :house_service_infras do
  end

  resources :service_infras do
  end

  resources :cloud_servers do
    member do
      get :check_updates

      post :set_cloud_server_recently_updated
    end
  end

  resources :icfn_nodes do
    member do
      get :check_updates

      post :set_icfn_node_recently_updated
    end

    collection do
      get :nearby_icfn_node
    end
  end

end