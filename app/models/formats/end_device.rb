module Formats::EndDevice

  def as_json( opts = {} )
    # => Order of keys important ....
    # => The first key's value will have the payload data encrypted with a symmetric key ...
    # => The second key's value will have the symmetric key in PKI encrypted manner ...

    # => Retrieve the security template ...
    security_template = opts[:current_security_template]

    return if security_template.nil?

    json_response = {
      symmetrically_encrypted_payload_data: Base64.encode64(security_template.symmetrically_encrypted_payload_data(self.plain_payload_data_hash)),
      asymmetrically_encrypted_symmetric_encryption_info: Base64.encode64(security_template.asymmetrically_encrypted_symmetric_encryption_info)
    }

    json_response
  end


  def plain_payload_data_hash(opts = {})
    {
      id: self.id, 
      name: self.name,
      house_section_id: self.house_section_id,
      end_device_info_id: self.end_device_info_id,
      service_infra_id: self.service_infra_id,
      device_gateway_id: self.device_gateway_id,
      should_alert_status: self.should_alert_status,
      serial_number: self.serial_number
    }
  end



end