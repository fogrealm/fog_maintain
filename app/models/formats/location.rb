module Formats::Location
  
  def as_json( opt = {} )
    {
      id: self.id,
      latitude: self.latitude, 
      longitude: self.longitude,
      locatable_type: self.locatable_type,
      locatable_id: self.locatable_id
    }
  end
  
end