module Formats::DeviceGateway

  def as_json( opts = {} )
    # => Order of keys important ....
    # => The first key's value will have the payload data encrypted with a symmetric key ...
    # => The second key's value will have the symmetric key in PKI encrypted manner ...

    # => Retrieve the security template ...
    security_template = opts[:current_security_template]

    return if security_template.nil?

    json_response = {
      symmetrically_encrypted_payload_data: Base64.encode64(security_template.symmetrically_encrypted_payload_data(self.plain_payload_data_hash)),
      asymmetrically_encrypted_symmetric_encryption_info: Base64.encode64(security_template.asymmetrically_encrypted_symmetric_encryption_info)
    }

    json_response
  end


  def plain_payload_data_hash(opts = {})
    {
      id: self.id, 
      name: self.name,
      device_gateway_url: self.device_gateway_url,
      base_url: self.base_url,
      house_id: self.house_id,
      channel_key: self.channel_key,
      channel_encryption_scheme: self.channel_encryption_scheme
    }
  end

end