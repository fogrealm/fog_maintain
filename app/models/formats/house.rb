module Formats::House

  def as_json( opts = {} )
    # => Order of keys important ....
    # => The first key's value will have the payload data encrypted with a symmetric key ...
    # => The second key's value will have the symmetric key in PKI encrypted manner ...

    # => Retrieve the security template ...
    security_template = opts[:current_security_template]

    return if security_template.nil?

    json_response = {
      symmetrically_encrypted_payload_data: Base64.encode64(security_template.symmetrically_encrypted_payload_data(self.plain_payload_data_hash)),
      asymmetrically_encrypted_symmetric_encryption_info: Base64.encode64(security_template.asymmetrically_encrypted_symmetric_encryption_info)
    }

    json_response
  end


  def plain_payload_data_hash(opts = {})
    nearby_icfn_node = self.nearby_icfn_node(IcfnNode::DEFAULT_ICFN_NODES_SEARCH_RADIUS)
    nearby_cloud_server = self.nearby_cloud_server

    {
      id: self.id,
      name: self.name,
      address: self.address,
      landmark: self.landmark, 
      description: self.description,
      authentication_token: self.authentication_token,
      location: self.location.as_json,

      nearby_icfn_node: {
        icfn_id: nearby_icfn_node.id,
        icfn_name: nearby_icfn_node.name,
        icfn_base_url: nearby_icfn_node.base_url,
        icfn_channel_key: self.icfn_channel_key(nearby_icfn_node)
      },

      nearby_cloud_server: {
        cloud_id: nearby_cloud_server.id,
        cloud_name: nearby_cloud_server.name,
        cloud_base_url: nearby_cloud_server.base_url,
        cloud_channel_key: self.cloud_channel_key(nearby_cloud_server)
      }
    }
  end
end