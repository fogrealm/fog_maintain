module Formats::IcfnNode

  def as_json(opts = {})
    {
      id: self.id,
      name: self.name, 
      base_url: self.base_url,
      running_status: self.running_status,
      is_recently_updated: self.is_recently_updated,
      location: self.location.as_json
    }
  end

end