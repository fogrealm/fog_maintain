module Formats::ServiceInfra

  def as_json( opts = {} )
    # => Order of keys important ....
    # => The first key's value will have the payload data encrypted with a symmetric key ...
    # => The second key's value will have the symmetric key in PKI encrypted manner ...

    # => Retrieve the security template ...
    # => Unlike the other models, we take the first ever security template from the database ...
    # => The above is quick fix as of now ...
    # => The change which needs to be made is to accept the house_id of the requesting house ...
    # => On doing the above change, we can obtain the house_id in the as_json method in order to retrieve the security_template ... 
    security_template = opts[:current_security_template]

    return if security_template.nil?

    json_response = {
      symmetrically_encrypted_payload_data: Base64.encode64(security_template.symmetrically_encrypted_payload_data(self.plain_payload_data_hash)),
      asymmetrically_encrypted_symmetric_encryption_info: Base64.encode64(security_template.asymmetrically_encrypted_symmetric_encryption_info)
    }

    json_response
  end


  def plain_payload_data_hash(opts = {})
    {
      id: self.id, 
      name: self.name,
      priority: self.priority
    }
  end

end