class CommunicationChannel < ApplicationRecord
  include Actions::CommunicationChannel
  include Attributes::CommunicationChannel

  # => Constants ...
  COMMUNICATION_CHANNEL_TYPE_HOUSE_TO_ICFN = "house_to_icfn"
  COMMUNICATION_CHANNEL_TYPE_HOUSE_TO_CLOUD = "house_to_cloud"
  COMMUNICATION_CHANNEL_TYPE_ICFN_TO_CLOUD = "icfn_to_cloud"

  COMMUNICATION_CHANNEL_TYPES = [
    COMMUNICATION_CHANNEL_TYPE_HOUSE_TO_ICFN,
    COMMUNICATION_CHANNEL_TYPE_HOUSE_TO_CLOUD,
    COMMUNICATION_CHANNEL_TYPE_ICFN_TO_CLOUD
  ]


  # => Relations ...
  belongs_to :transmitter_communication_channelable, polymorphic: true
  belongs_to :receiver_communication_channelable, polymorphic: true


  # => Validations ...
  validates :name, :channel_key, presence: true
  validates :transmitter_communication_channelable_id, :receiver_communication_channelable_id, presence: true
  validates_uniqueness_of :transmitter_communication_channelable_id, scope: [:transmitter_communication_channelable_type, :receiver_communication_channelable_id, :receiver_communication_channelable_type]
  validates_uniqueness_of :channel_key
  validates :communication_channel_type, inclusion: { :in => COMMUNICATION_CHANNEL_TYPES }


  # => Scopes ...
  scope :communication_channel_type_house_to_icfn, -> {
    where(
      :communication_channel_type => COMMUNICATION_CHANNEL_TYPE_HOUSE_TO_ICFN
    )
  }

  scope :communication_channel_type_house_to_cloud, -> {
    where(
      :communication_channel_type => COMMUNICATION_CHANNEL_TYPE_HOUSE_TO_CLOUD
    )
  }

  scope :communication_channel_type_icfn_to_cloud, -> {
    where(
      :communication_channel_type => COMMUNICATION_CHANNEL_TYPE_ICFN_TO_CLOUD
    )
  }

  
  # => Callbacks ...
  before_validation do
    # => pre fill attribute values ...
    # => The IDs of the transmitter and receiver nodes are supposed to be filled the fog maintenance server admin ...

    if self.communication_channel_type == COMMUNICATION_CHANNEL_TYPE_HOUSE_TO_ICFN
      self.transmitter_communication_channelable_type = House.to_s
      self.receiver_communication_channelable_type = IcfnNode.to_s
    elsif self.communication_channel_type == COMMUNICATION_CHANNEL_TYPE_HOUSE_TO_CLOUD
      self.transmitter_communication_channelable_type = House.to_s
      self.receiver_communication_channelable_type = CloudServer.to_s
    elsif self.communication_channel_type == COMMUNICATION_CHANNEL_TYPE_ICFN_TO_CLOUD
      self.transmitter_communication_channelable_type = IcfnNode.to_s
      self.receiver_communication_channelable_type = CloudServer.to_s
    end    

    if self.channel_key.blank?
      self.channel_key = generate_new_channel_key
    end

    if self.name.blank?
      self.name = self.default_name
    end
  end

end