class CloudServer < ApplicationRecord
  include Actions::CloudServer

  # => Relations ...
  has_one :security_template, as: :security_templatable, :dependent => :destroy

  has_many :platform_errors, as: :platform_errorable, dependent: :destroy


  # => Validations ...
  validates :name, presence: true
  validates :base_url, presence: true


  # => Associations ...
  accepts_nested_attributes_for :security_template


  # => Callbacks ...
  before_validation on: :create do
    # => We prefill some of the attributes ...

    if self.running_status.nil?
      self.running_status = false
    end

    if self.is_recently_updated.nil?
      self.is_recently_updated = true
    end

    if self.authentication_token.blank?
      self.authentication_token = generate_new_auth_token
    end
  end
  
end
