class SecurityTemplate < ApplicationRecord
  #include Formats::SecurityTemplate

  # => Constants ...
  DEFAULT_SYMMETRIC_ENCRYPTION_SCHEME = "AES-128-CBC"
  
  # => Please note that these same specs are used for fog node, icfn and cloud server ...
  DEFAULT_HOUSE_SYMMETRIC_ENCRYPTION_SCHEME = "AES-192-CBC"
  DEFAULT_HOUSE_SYMMETRIC_ENCRYPTION_KEY_SIZE_BYTES = 24
  

  # => Relations ...
  belongs_to :security_templatable, polymorphic: true


  # => Validations ...
  validates :name, presence: true
  validates :symmetric_encryption_scheme, presence: true
  validates :private_key, :public_key, presence: true
  validates :symmetric_encryption_scheme, inclusion: { :in => OpenSSL::Cipher.ciphers }
  validates_uniqueness_of :security_templatable_id, scope: :security_templatable_type


  # => Accessors ...
  attr_accessor :symmetric_key, :initial_vector


  # => Callbacks ...  
  before_validation on: :create do
    # => We prefill the security template information ...
    # => The information is filled on the basis whether the entity to be secured is a house or icfn node ...

    if self.security_templatable_type == House.to_s
      self.name = "house_#{self.security_templatable_id}_security_template"
      ssh_key_comment = "house_#{self.security_templatable_id}@fog_maintain.com"
      ssh_key_passphrase = "house_#{self.security_templatable_id}"
    elsif self.security_templatable_type == IcfnNode.to_s
      self.name = "icfn_node_#{self.security_templatable_id}_security_template"
      ssh_key_comment = "icfn_node_#{self.security_templatable_id}@fog_maintain.com"
      ssh_key_passphrase = "icfn_node_#{self.security_templatable_id}"
    elsif self.security_templatable_type == CloudServer.to_s
      self.name = "cloud_server_#{self.security_templatable_id}_security_template"
      ssh_key_comment = "cloud_server_#{self.security_templatable_id}@fog_maintain.com"
      ssh_key_passphrase = "cloud_server_#{self.security_templatable_id}"
    end

    self.symmetric_encryption_scheme = DEFAULT_SYMMETRIC_ENCRYPTION_SCHEME if self.symmetric_encryption_scheme.nil?
    
    ssh_key = SSHKey.generate(
          type:       "RSA",
          bits:       2048,
          comment:    ssh_key_comment,
          passphrase: ssh_key_passphrase
        )

    self.private_key = Base64.encode64(ssh_key.private_key)
    self.public_key = Base64.encode64(ssh_key.public_key)
  end


  def symmetric_encryption_info
    {
      symmetric_encryption_scheme: self.symmetric_encryption_scheme,
      symmetric_key: Base64.encode64(self.symmetric_key),
      initial_vector: Base64.encode64(self.initial_vector)
    }
  end

  
  def symmetrically_encrypted_payload_data(plain_payload_data_hash)
    cipher = OpenSSL::Cipher.new(self.symmetric_encryption_scheme)
    cipher.encrypt

    self.symmetric_key = cipher.random_key
    self.initial_vector = cipher.random_iv    

    plain_payload_data = plain_payload_data_hash.to_json

    encrypted_payload_data = cipher.update(plain_payload_data) + cipher.final
    encrypted_payload_data    
  end

  
  def asymmetrically_encrypted_symmetric_encryption_info
    
    public_key = OpenSSL::PKey::RSA.new(Base64.decode64(self.public_key))
    symmetric_encryption_info_hash = self.symmetric_encryption_info
    encrypted_string = Base64.encode64(public_key.public_encrypt(symmetric_encryption_info_hash.to_json))
    encrypted_string
  end


  def symmetrically_decrypt_payload_data(key_size_bytes, symmetric_encryption_scheme, request_entity_type, encrypted_request_payload = {})
    # => This code will symmetrically decrypt the payload data received from house, icfn or cloud server ...
    # => As of now, both house and icfn request entity type uses the same encryption scheme and key size ...
    # => So, 'type' argument does not hold much signifance here as both have similar attributes. Ex: authentication_token

    key_size_bytes ||= DEFAULT_HOUSE_SYMMETRIC_ENCRYPTION_KEY_SIZE_BYTES
    symmetric_encryption_scheme ||= DEFAULT_HOUSE_SYMMETRIC_ENCRYPTION_SCHEME

    base64_decoded_encrypted_request_payload_data = Base64.decode64(encrypted_request_payload)
    request_entity_authentication_token = self.security_templatable.authentication_token
    key = request_entity_authentication_token.ljust(key_size_bytes, '0')

    decipher = OpenSSL::Cipher.new(symmetric_encryption_scheme)
    decipher.decrypt
    decipher.key = key
    
    request_payload_data = decipher.update(base64_decoded_encrypted_request_payload_data) + decipher.final
    
    JSON.parse(request_payload_data)
  end


  def self.retrieve_most_trustworthy_icfn_node(scoped_icfn_nodes = nil)
    # => We will select the port with least failure experience out of the last 10 failure events ...

    scoped_icfn_nodes ||= IcfnNode.all
    scoped_icfn_node_ids = scoped_icfn_nodes.map(&:id)
    failed_icfn_node_ids = PlatformError.platform_error_type_icfn_failure.map(&:platform_errorable_id)

    unfailed_icfn_node_ids = scoped_icfn_node_ids - failed_icfn_node_ids.uniq
    
    result_icfn_node_id = unfailed_icfn_node_ids.first
    result_icfn_node = nil

    if result_icfn_node_id.nil?
      icfn_node_id_frequency_hash = {}
    
      failed_icfn_node_ids.each do |failed_icfn_node_id|
        icfn_node_id_frequency_hash[failed_icfn_node_id] = 0 if icfn_node_id_frequency_hash[failed_icfn_node_id].nil?
        icfn_node_id_frequency_hash[failed_icfn_node_id] += 1
      end
      result_icfn_node_id = failed_icfn_node_ids.min_by{|key| icfn_node_id_frequency_hash[key]}
    end

    result_icfn_node = scoped_icfn_nodes.where(id: result_icfn_node_id).first
    result_icfn_node
  end
end
