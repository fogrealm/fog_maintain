class House < ApplicationRecord
  include ActiveRecordBaseCommon::Validations
  include Formats::House
  include Conditions::House
  include Actions::House
  include Attributes::House

  # => Relations ...
  has_many :users, :dependent => :destroy
  has_many :house_sections, :dependent => :destroy

  has_many :house_service_infras, :dependent => :destroy
  has_many :service_infras, :through => :house_service_infras
  
  has_many :end_devices, :through => :house_sections
  has_many :platform_errors, :dependent => :destroy

  has_many :device_gateways, :dependent => :destroy

  has_many :transmitter_communication_channels, as: :transmitter_communication_channelable, class_name: CommunicationChannel.to_s, :dependent => :destroy
  has_many :receiver_communication_channels, as: :receiver_communication_channelable, class_name: CommunicationChannel.to_s, :dependent => :destroy

  has_many :platform_errors, as: :platform_errorable, dependent: :destroy

  has_one :location, as: :locatable, :dependent => :destroy
  has_one :security_template, as: :security_templatable, :dependent => :destroy


  # => Validations ...
  validates :name, :address, :landmark, :authentication_token, presence: true
  validates :location, :security_template, has_one: true
  
  validates_uniqueness_of :authentication_token


  # => Associations ...
  accepts_nested_attributes_for :location
  accepts_nested_attributes_for :security_template
  accepts_nested_attributes_for :house_sections, :allow_destroy => true


  # => Callbacks ...
  before_validation on: :create do
    # => pre fill attribute values ...

    if self.is_recently_updated.nil?
      self.is_recently_updated = false
    end

    if self.authentication_token.blank?
      self.authentication_token = generate_new_auth_token
    end
  end


  def changer_man
    hs = HouseSection.first
    hs.name = "kitchen2"
    hs.save!
  
    ip = InfraParameter.first
    ip.name = "try"
    ip.save!

    h = House.first
    h.address = "no address"
    h.is_recently_updated = false
    h.save!

    h = IcfnNode.first
    h.is_recently_updated = false
    h.save!

    h = CloudServer.first
    h.is_recently_updated = false
    h.save!
  end


  def encrypt(data = "", key = nil, iv = nil, scheme = 'AES-128-CBC')
    # => 

    scheme = 'AES-128-CBC' if scheme.nil?
    puts "\n scheme - #{scheme}"
    cipher = OpenSSL::Cipher.new(scheme)
    cipher.encrypt

    key = cipher.random_key if key.nil?
    iv = cipher.random_iv if iv.nil?

    encrypted_data = cipher.update(data) + cipher.final

    encrypted_content = {
      :encrypted_data => encrypted_data,
      :key => key,
      :iv => iv,
      :scheme => scheme
    }
  end

  def decrypt(encrypted_content)
    scheme = encrypted_content[:scheme]
    key = encrypted_content[:key]
    iv = encrypted_content[:iv]
    encrypted_data = encrypted_content[:encrypted_data]

    decipher = OpenSSL::Cipher.new(scheme)
    decipher.decrypt
    decipher.key = key
    decipher.iv = iv

    plain_data = decipher.update(encrypted_data) + decipher.final
    
    decrypted_content = {
      :plain_data => plain_data,
      :key => key,
      :iv => iv,
      :scheme => scheme
    }
  end



end
