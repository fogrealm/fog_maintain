class IcfnNode < ApplicationRecord
  include Formats::IcfnNode
  include Actions::IcfnNode

  # => Constants ...
  DEFAULT_ICFN_NODES_SEARCH_RADIUS = 10

  # => Relations ...
  has_one :location, as: :locatable, :dependent => :destroy
  has_one :security_template, as: :security_templatable, :dependent => :destroy

  has_many :platform_errors, as: :platform_errorable, dependent: :destroy
  

  # => Validations ...
  validates :name, presence: true
  validates :base_url, presence: true


  # => Associations ...
  accepts_nested_attributes_for :location, :security_template


  # => Callbacks ...
  before_validation on: :create do
    # => We prefill some of the attributes ...

    if self.running_status.nil?
      self.running_status = false
    end

    if self.authentication_token.blank?
      self.authentication_token = generate_new_auth_token
    end
  end
  
end