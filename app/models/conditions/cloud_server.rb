module Conditions::CloudServer

  def is_recently_updated?
    # => Method to check whether the house is recently updated to fog, icfn and cloud ...

    self.is_recently_updated == true    
  end

end