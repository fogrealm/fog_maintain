module Attributes::House
  
  def nearby_icfn_node(radius = IcfnNode::DEFAULT_ICFN_NODES_SEARCH_RADIUS)
    # => This function will return any nearby icfn node within this house's location visibility ...

    house_latitude = self.location.latitude
    house_longitude = self.location.longitude

    nearby_icfn_node_ids = Location.locatable_type_icfn_node.near([house_latitude, house_longitude], radius).map(&:locatable_id)
    nearby_icfn_nodes  = IcfnNode.where(id: nearby_icfn_node_ids)  
    SecurityTemplate.retrieve_most_trustworthy_icfn_node(nearby_icfn_nodes)
  end


  def nearby_cloud_server
    # => As of now, we are returning the only cloud server stored in our fog maintenace server ...

    CloudServer.first
  end


  def icfn_channel_key(icfn_node = nil)
    # => This method retrieves the channel key of the communication channel connecting this house and the icfn node mentioned in the arguement ...
    # => If not Icfn node is mentioned in the arguement, we again compute a nearby icfn node to retrieve it's channel key ...

    icfn_node = self.nearby_icfn_node(10) if icfn_node.nil?
    house_communication_channels = self.transmitter_communication_channels
    receiver_type_icfn_house_communication_channels = house_communication_channels.communication_channel_type_house_to_icfn 
    
    receiver_type_icfn_house_communication_channels.where(receiver_communication_channelable_id: icfn_node.id).first.try(:channel_key)
  end


  def cloud_channel_key(cloud_server = nil)
    # => This method retrieves the channel key of the communication channel connecting this house and the cloud server mentioned in the arguement ...
    # => If not Icfn node is mentioned in the arguement, we consider the already cloud server retrieval method to obtain it's channel key ...

    cloud_server = self.nearby_cloud_server if cloud_server.nil?
    house_communication_channels = self.transmitter_communication_channels
    receiver_type_cloud_house_communication_channels = house_communication_channels.communication_channel_type_house_to_cloud
    
    receiver_type_cloud_house_communication_channels.where(receiver_communication_channelable_id: cloud_server.id).first.try(:channel_key)
  end

end