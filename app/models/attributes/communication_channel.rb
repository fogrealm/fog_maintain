module Attributes::CommunicationChannel

  def default_name
    # => The Default name for a communication channel that could be generated ...
    # => Recommended as it handles the issue of uniquess in naming communication channel ...

    "#{self.transmitter_communication_channelable_type}_#{self.transmitter_communication_channelable_id}_to_#{self.receiver_communication_channelable_type}_#{self.receiver_communication_channelable_id}"
  end
end