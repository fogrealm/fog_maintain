class PlatformError < ApplicationRecord

  # => Constants ...
  PLATFORM_ERROR_TYPE_FOG_FAILURE = "fog_failure"
  PLATFORM_ERROR_TYPE_ICFN_FAILURE = "icfn_failure"
  PLATFORM_ERROR_TYPE_CLOUD_FAILURE = "cloud_failure"

  PLATFORM_ERROR_TYPES = [
    PLATFORM_ERROR_TYPE_FOG_FAILURE,
    PLATFORM_ERROR_TYPE_ICFN_FAILURE,
    PLATFORM_ERROR_TYPE_CLOUD_FAILURE
  ]


  # => Relations ...
  belongs_to :platform_errorable, polymorphic: true


  # => Validations ...
  validates :platform_error_type, inclusion: { :in => PLATFORM_ERROR_TYPES }
  validates :reported_at, presence: true
  

  # => Accessors ...
  attr_accessor :reported_at_timestamp


  # => Scopes ...
  scope :platform_error_type_fog_failure, -> {
    where(platform_error_type: PLATFORM_ERROR_TYPE_FOG_FAILURE)
  }

  scope :platform_error_type_icfn_failure, -> {
    where(platform_error_type: PLATFORM_ERROR_TYPE_ICFN_FAILURE)
  }

  scope :platform_error_type_cloud_failure, -> {
    where(platform_error_type: PLATFORM_ERROR_TYPE_CLOUD_FAILURE)
  }


  # => Callbacks ...
  before_validation on: :create do
    # => pre fill the attributes of platform error ...

    if self.name.blank?
      self.name = "platform_error_#{self.id}_#{platform_error_type}_#{platform_errorable_type}"
    end

    if self.reported_at_timestamp.present?
      self.reported_at = Time.at(self.reported_at_timestamp)
    end
  end

end
