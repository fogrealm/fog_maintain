class User < ApplicationRecord
  include Formats::User

  # => Relations ...
  belongs_to :house

  # => Validations ...
  validates :name, :house, presence: true
  validates :phone_number, presence: true, format: { with: ActiveRecordBaseCommon::Validations::VALID_PHONE_FORMAT }
  
  validates_uniqueness_of :phone_number

end
