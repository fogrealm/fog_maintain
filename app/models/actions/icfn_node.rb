module Actions::IcfnNode
  
  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
  end
  

  module ClassMethods
      
  end
  
  
  module InstanceMethods
    
    def set_recently_updated!
      # => This method will change the status of this as recently updated ...

      self.is_recently_updated = true
      self.save!
    end

    def reset_authentication_token
      # => This method reset the authentication token to a new one ...

      self.authentication_token = generate_new_auth_token
      self.save!
    end

    def generate_new_auth_token
      # => This method generate a new authentication token ...
    
      token = Devise.friendly_token
      while IcfnNode.where(authentication_token: token).count > 0
        token = Devise.friendly_token
      end

      token
    end
  end
  
  
end