module Actions::House
  
  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
  end
  

  module ClassMethods
      
  end
  
  
  module InstanceMethods
    
    def set_recently_updated!
      # => This method will change the status of this as recently updated ...

      self.is_recently_updated = true
      self.save!
    end

    
    def reset_authentication_token
      # => This method reset the authentication token to a new one ...

      self.authentication_token = generate_new_auth_token
      self.save!
    end

    
    def generate_new_auth_token
      # => This method generate a new authentication token ...
    
      token = Devise.friendly_token
      while House.where(authentication_token: token).count > 0
        token = Devise.friendly_token
      end

      token
    end

    
    def inform_authentication_token_to_admin_users
      
      house_users_phone_numbers = self.users.map(&:phone_number)
      Rails.logger.info "\n Informing authentication_token -> #{self.authentication_token} to #{self.users.map(&:phone_number)}"

      SmsManager.send_sms(
        {
          to: house_users_phone_numbers, 
          message: "Note down this secret authentication token. Update this in your admin panel"
        }
      )
    end
  end

end