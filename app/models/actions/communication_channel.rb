module Actions::CommunicationChannel
  
  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
  end
  

  module ClassMethods
    
  end
  
  
  module InstanceMethods
    
    def reset_channel_key
      # => This method reset the authentication token to a new one ...

      self.channel_key = generate_new_channel_key
      self.save!
    end

    
    def generate_new_channel_key
      # => This method generate a new channel key ...
    
      key = Devise.friendly_token
      while CommunicationChannel.where(channel_key: key).count > 0
        key = Devise.friendly_token
      end

      key
    end
  
  end

end