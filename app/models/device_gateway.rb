class DeviceGateway < ApplicationRecord
  include Formats::DeviceGateway
  include Actions::DeviceGateway
  
  # => Constants ...
  DEFAULT_CHANNEL_ENCRYPTION_SCHEME = "AES-192-CBC"

 
  # => Relations ...
  has_many :end_devices

  belongs_to :house

 
  # => Validations ...
  validates :name, presence: true
  validates :base_url, presence: true
  validates :channel_encryption_scheme, inclusion: { :in => OpenSSL::Cipher.ciphers }

  validates_uniqueness_of :channel_key
  
 
  # => Callbacks ...
  before_validation on: :create do
    # => prefill the attribute values ...

    self.id = DeviceGateway.maximum(:id).to_i.next if self.id.nil?

    if self.channel_key.blank?
      self.channel_key = generate_new_channel_key
    end

    if self.channel_encryption_scheme.nil?
      self.channel_encryption_scheme = DEFAULT_CHANNEL_ENCRYPTION_SCHEME
    end
  end


  after_save :generate_device_gateway_url#, :if => :if_check_generate_device_gateway_aftersave_callback?

 
  # => Accessors
  attr_accessor :if_check_generate_device_gateway_aftersave_callback_var
  
  private

  def generate_device_gateway_url
    # => We generate device_gateway_url for this device_gateway based on its ID ...

    device_gateway_url = "#{self.base_url}/device_gateways/#{self.id}"
    #self.save(:validate => false)
    self.update_columns(device_gateway_url: device_gateway_url)
  end


end
