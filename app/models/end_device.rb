class EndDevice < ApplicationRecord
  include Formats::EndDevice
  #include Conditions::EndDevice

  # => Relations ...
  has_many :house_events
  has_many :infra_parameters, :through => :end_device_info 
  has_many :infra_readings
  
  belongs_to :end_device_info
  belongs_to :house_section
  belongs_to :service_infra
  #belongs_to :house_service_infra
  belongs_to :device_gateway


  # => Validations ...
  validates :name, presence: true
  validates :end_device_info, :house_section, :device_gateway, presence: true
  validates :should_alert_status, inclusion: { :in => [ true, false ] }
  validates :serial_number, presence: true
  
  validates_uniqueness_of :serial_number

  validate :ensure_house_section_and_service_infra_is_valid
  
  
  # => Delegations ...
  delegate :house, to: :house_section

  # useful for fast creation of end devices in admin panel ...
  # before_validation do
  #  self.name = "#{self.end_device_info.name} Device  #{self.house_section.name}"
  # end


  private

  def ensure_house_section_and_service_infra_is_valid
    # => We check whether the corresponding house has this service infra ...
    # => This should be done in order to check we are assigning to a valid service infra for a house ...
    # => We check whether the house section is valid ...

    house = nil

    if self.house_section.present?
      house = self.house_section.house
    end

    if house.nil?
      errors[:end_device] << " cannot be created because, the house id is invalid for this corresponding house section. "
      return
    end
    
    service_infra_ids = house.service_infras.map(&:id)

    if service_infra_ids.include?(self.service_infra_id) == false
      errors[:end_device] << " cannot be created because, this corresponding house has no such service infra. Please check this by filtering house service infra entity."
      return
    end
  end
end