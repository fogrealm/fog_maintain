class SmsManager

  CONFIG = YAML.load_file("#{::Rails.root}/config/sms.yml")[::Rails.env]
  DEFAULT_COUNTRY_CODE = 91
  DEFAULT_RESPONSE_FORMAT = 'json'

  def self.connection
    @conn ||=
        Faraday.new(:url => CONFIG['service_url_root'],  ssl: { verify: false }) do |faraday|
          faraday.request  :url_encoded             # form-encode POST params
          faraday.response :logger                  # log requests to STDOUT
          faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
        end
  end

  
  class << self
    
    # method to send a push notification to a user or a channel
    # accepted params
    #   message - message to show on notification bar
    #   to - phone numbers of users to send the push notification.
    #   expire_at - when will the push notification expire
    
    def send_sms(params)
      payload = {}
      payload[:method] = 'sms'
      payload[:format] = 'json'
      payload[:sender] = CONFIG['sender_id']
      payload[:authkey] = CONFIG['auth_key']
      payload[:mobiles] = params[:to].join(',')
      payload[:message] = params[:message]
      payload[:route] = CONFIG['route']
      payload[:country] = DEFAULT_COUNTRY_CODE
      payload[:response] = DEFAULT_RESPONSE_FORMAT

      puts CONFIG['service_url_root'] + CONFIG['service_url_path']
      Delayed::Worker.logger.debug("\n\n payload -> #{payload} \n\n")     

      response = connection.get do |req|
        req.url CONFIG['service_url_path']
        req.headers['Content-Type'] = 'application/json'
        req.params = payload
      end

      Delayed::Worker.logger.debug("\n\n  response -> #{response} \n\n")     
      

      unless JSON.parse(response.body)['type'] == 'success'
        Rails.logger.info "\n Message cannot be send "
      end
    end

    
    handle_asynchronously :send_sms
  end
end