class HouseEventsController < InheritedResources::Base
  respond_to :json


  private

  def permitted_params
    params.permit(house_event: [:id, :name, :event_level, :house_event_type, :end_device_id, :reported_at])
  end

end