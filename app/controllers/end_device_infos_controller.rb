class EndDeviceInfosController < InheritedResources::Base
  respond_to :json

  def index
    super do |format|
      format.json {
        render json: @end_device_infos.as_json({
          current_security_template: @current_security_template
        })
      }
    end
  end


  def show
    super do |format|
      format.json {
        render json: @end_device_info.as_json({
          current_security_template: @current_security_template
        })
      }
    end
  end

end
