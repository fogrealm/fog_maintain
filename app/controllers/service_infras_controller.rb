class ServiceInfrasController < InheritedResources::Base
  respond_to :json
  belongs_to :house, optional: true

  def index
    super do |format|
      format.json {
        render json: @service_infras.as_json({
          current_security_template: @current_security_template
        })
      }
    end
  end


  def show
    super do |format|
      format.json {
        render json: @service_infra.as_json({
          current_security_template: @current_security_template
        })
      }
    end
  end

end
