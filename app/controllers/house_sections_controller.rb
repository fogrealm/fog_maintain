class HouseSectionsController < InheritedResources::Base
  respond_to :json

  belongs_to :house


  def index
    super do |format|
      format.json {
        render json: @house_sections.as_json({
          current_security_template: @current_security_template
        })
      }
    end
  end


  def show
    super do |format|
      format.json {
        render json: @house_section.as_json({
          current_security_template: @current_security_template
        })
      }
    end
  end


  private

  def permitted_params
    params.permit(house_section: [:id, :name])
  end
end
