class HouseServiceInfrasController < InheritedResources::Base
  respond_to :json

  def index
    super do |format|
      format.json {
        render json: @house_service_infras.as_json({
          current_security_template: @current_security_template
        })
      }
    end
  end


  def show
    super do |format|
      format.json {
        render json: @house_service_infra.as_json({
          current_security_template: @current_security_template
        })
      }
    end
  end

end
