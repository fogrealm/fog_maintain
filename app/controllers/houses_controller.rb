class HousesController < InheritedResources::Base
  respond_to :json


  def index
    super do |format|
      format.json {
        render json: @houses.as_json({
          current_security_template: @current_security_template
        })
      }
    end
  end


  def show
    super do |format|
      format.json {
        render json: @house.as_json({
          current_security_template: @current_security_template
        })
      }
    end
  end


  def check_updates
    
    @house = House.find(params[:id])# rescue raise ActionControllerBaseCommon::InvalidParametersException and return

    render json: {
      status: 200,
      updation_status: {
        is_recently_updated: @house.is_recently_updated?
      }
    } and return 
  end


  def set_house_recently_updated
    @house = House.find(params[:id])# rescue raise ActionControllerBaseCommon::InvalidParametersException and return

    @house.set_recently_updated!

    render json: {
      updation_status: {
        is_recently_updated: @house.is_recently_updated?
      }
    } and return
  end

  private

  def permitted_params
    params.permit(house: [:id, :name, :address, :landmark, :description, location_attributes: [:id, :latitude, :longitude], house_sections_attributes: [:id, :name, :_destroy]])
  end

end
