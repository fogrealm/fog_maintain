class InfraParametersController < InheritedResources::Base
  respond_to :json

  def index
    super do |format|
      format.json {
        render json: @infra_parameters.as_json({
          current_security_template: @current_security_template
        })
      }
    end
  end


  def show
    super do |format|
      format.json {
        render json: @infra_parameter.as_json({
          current_security_template: @current_security_template
        })
      }
    end
  end

end