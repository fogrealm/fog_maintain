class PlatformErrorsController < ApplicationController
  respond_to :json


  private

  def permitted_params
    params.permit(platform_error: [:id, :house_id, :platform_error_type, :reported_at_timestamp])
  end

end
