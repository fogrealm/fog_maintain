class DeviceGatewaysController < InheritedResources::Base
  respond_to :json
  belongs_to :house

  def index
    super do |format|
      format.json {
        render json: @device_gateways.as_json({
          current_security_template: @current_security_template
        })
      }
    end
  end


  def show
    super do |format|
      format.json {
        render json: @device_gateway.as_json({
          current_security_template: @current_security_template
        })
      }
    end
  end

end
