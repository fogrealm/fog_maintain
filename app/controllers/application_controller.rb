class ApplicationController < ActionController::Base
  
  respond_to :html, :json

  # this enables DSL supported by inherited resource.
  # use it to override actions after actions
  include InheritedResources::DSL
  include ActionControllerBaseCommon::Exceptions
  include ActionControllerBaseCommon::DeviseAuthentications

  # collection override for inherited resource
  # this adds pagination automatically for all index actions
  # def collection
  #   get_collection_ivar || set_collection_ivar(end_of_association_chain.page(params[:page]).per(25))
  # end

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  before_action :authenticate_request_payload
  before_action :set_current_security_template

  after_filter do
    cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
  end
  
  def verified_request?
    # super || form_authenticity_token == request.headers['X-XSRF-TOKEN'] Rails 4.1 & below
    super || valid_authenticity_token?(session, request.headers['X-XSRF-TOKEN'])
  end


  protected

  def apply_has_scopes
    model_name = controller_name.classify.constantize
    resource_collection_variable_string = model_name.to_s.downcase.pluralize

    eval("
      @#{resource_collection_variable_string} ||= #{model_name}.all
      @#{resource_collection_variable_string} = apply_scopes(@#{resource_collection_variable_string}).all
    ")
  end


  def set_member_resource_variable
    model_name = controller_name.classify.constantize
    eval("@#{model_name.to_s.downcase} = #{model_name}.find(params[:id])")
  end


  def authenticate_request_payload
    if params[:ignore_security] == true.to_s || params[:ignore_security] == true
      return true
    end
    
    if request.format == :json 
      if is_fog_request_payload_valid?(params) == true
        Rails.logger.info "\n Token validation successful :)"
      else
        raise CanCan::AccessDenied
      end
    end
  end


  def set_current_security_template
    return if params[:layer].nil? || params[:layer].to_i == 0

    layer = params[:layer].to_i 

    @current_security_template = nil
    if layer == 1 
      return if params[:house_id].nil? || params[:house_id].to_i == 0

      house_id = params[:house_id].to_i
      @current_security_template = House.find(house_id).security_template
    elsif layer == 3
      return if params[:icfn_id].nil? || params[:icfn_id].to_i == 0

      icfn_id = params[:icfn_id].to_i
      @current_security_template = IcfnNode.find(icfn_id).security_template
    elsif layer == 4
      return if params[:cloud_id].nil? || params[:cloud_id].to_i == 0

      cloud_id = params[:cloud_id].to_i
      @current_security_template = CloudServer.find(cloud_id).security_template
    end

    puts "\n\n current_security_template - #{@current_security_template}"

    @current_security_template
  end 


  def is_fog_request_payload_valid?(payload_params = nil)
    return false if payload_params.nil?
    return false if payload_params[:layer].nil?

    layer = payload_params[:layer].to_i
    encrypted_request_payload = payload_params[:encrypted_request_payload]
    fog_request_payload_validation_status = false

    Rails.logger.info "\n layer, encrypted_request_payload - #{layer}, #{encrypted_request_payload}"

    if layer == 1
      house_id = payload_params[:house_id].to_i
      return false if payload_params[:house_id].nil?

      requested_house = House.where(id: house_id).first
      
      if requested_house.present?
        request_payload_data_hash = requested_house.security_template.symmetrically_decrypt_payload_data(nil, nil, House.to_s, encrypted_request_payload)
        Rails.logger.info "\n Request Payload Data Hash - #{request_payload_data_hash}"

        fog_request_payload_validation_status = is_request_payload_data_hash_valid?(request_payload_data_hash, requested_house)
      end
    elsif layer == 3
      icfn_id = payload_params[:icfn_id].to_i
      return false if payload_params[:icfn_id].nil?

      requested_icfn_node = IcfnNode.where(id: icfn_id).first

      if requested_icfn_node.present?
        request_payload_data_hash = requested_icfn_node.security_template.symmetrically_decrypt_payload_data(nil, nil, IcfnNode.to_s, encrypted_request_payload)
        Rails.logger.info "\n Request Payload Data Hash - #{request_payload_data_hash}"

        fog_request_payload_validation_status = is_request_payload_data_hash_valid?(request_payload_data_hash, requested_icfn_node)
      end
    elsif layer == 4
      cloud_id = payload_params[:cloud_id].to_i
      return false if payload_params[:cloud_id].nil?

      requested_cloud_server = CloudServer.where(id: cloud_id).first
      
      if requested_cloud_server.present?
        request_payload_data_hash = requested_cloud_server.security_template.symmetrically_decrypt_payload_data(nil, nil, CloudServer.to_s, encrypted_request_payload)
        Rails.logger.info "\n Request Payload Data Hash - #{request_payload_data_hash}"

        fog_request_payload_validation_status = is_request_payload_data_hash_valid?(request_payload_data_hash, requested_cloud_server)
      end
    end

    fog_request_payload_validation_status
  end


  def is_request_payload_data_hash_valid?(request_payload_data_hash = nil, request_entity)
    return false if request_payload_data_hash.nil?
    return false if request_entity.nil? || ![IcfnNode.to_s, House.to_s, CloudServer.to_s].include?(request_entity.class.to_s)
    
    plain_message = nil
    if request_entity.is_a?(House)
      plain_message = {
        house_id: request_entity.id,
        house_authentication_token: request_entity.authentication_token
      }.to_json
    elsif request_entity.is_a?(IcfnNode)
      plain_message = {
        icfn_id: request_entity.id,
        icfn_authentication_token: request_entity.authentication_token
      }.to_json
    elsif request_entity.is_a?(CloudServer)
      plain_message = {
        cloud_id: request_entity.id,
        cloud_authentication_token: request_entity.authentication_token
      }.to_json
    end

    return false if plain_message.nil?
    
    base64_encoded_token_signature_received = request_payload_data_hash["encoded_token_signature"]
    
    token_signature_received = Base64.decode64(base64_encoded_token_signature_received)
    Rails.logger.info "\n token signature received - #{token_signature_received}"
    Rails.logger.info "\n plain message from receiver side - #{plain_message}"

    public_key_content = Base64.decode64(request_entity.security_template.public_key)

    public_key = OpenSSL::PKey::RSA.new(public_key_content)
    public_key.verify(OpenSSL::Digest::SHA256.new, token_signature_received, plain_message)
  end
end
