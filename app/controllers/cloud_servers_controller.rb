class CloudServersController < ApplicationController
  respond_to :json


  def check_updates
    
    @cloud_server = CloudServer.find(params[:id])# rescue raise ActionControllerBaseCommon::InvalidParametersException and return

    render json: {
      status: 200,
      updation_status: {
        is_recently_updated: @cloud_server.is_recently_updated?
      }
    } and return
  
  end


  def set_cloud_server_recently_updated
    @cloud_server = CloudServer.find(params[:id])# rescue raise ActionControllerBaseCommon::InvalidParametersException and return

    @cloud_server.set_recently_updated!

    render json: {
      updation_status: {
        is_recently_updated: @cloud_server.is_recently_updated?
      }
    } and return


  end
end
