class IcfnNodesController < ApplicationController
  respond_to :json

  def check_updates
    
    @icfn_node = IcfnNode.find(params[:id])# rescue raise ActionControllerBaseCommon::InvalidParametersException and return

    render json: {
      status: 200,
      updation_status: {
        is_recently_updated: @icfn_node.is_recently_updated?
      }
    } and return
  
  end


  def set_icfn_node_recently_updated
    @icfn_node = IcfnNode.find(params[:id])# rescue raise ActionControllerBaseCommon::InvalidParametersException and return

    @icfn_node.set_recently_updated!

    render json: {
      updation_status: {
        is_recently_updated: @icfn_node.is_recently_updated?
      }
    } and return
  end


  def nearby_icfn_node
    # => Please note it is not returning the nearest icfn node ...
    # => But any one within the vicinity ...

    obtained_icfn_node = nil

    if params[:house_latitude].present? && params[:house_longitude].present?
      house_latitude = params[:house_latitude].to_f
      house_longitude = params[:house_longitude].to_f
      radius = 100
      
      nearby_icfn_node_ids = Location.locatable_type_icfn_node.near([house_latitude, house_longitude], radius).map(&:locatable_id)
      obtained_icfn_node  = IcfnNode.where(id: 1).first
      
      if obtained_icfn_node.nil?
        render json: {
          error: {
            :message => "No Icfn Available"
          }
      } and return
      else
        render json: {
          icfn_node: obtained_icfn_node.as_json
        } and return
      end    
    else
      render json: {
        error: {
          :message => "invalid coordinates"   
        }
      } and return
    end
  end

end
