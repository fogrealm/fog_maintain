ActiveAdmin.register CommunicationChannel do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  permit_params :name, :communication_channel_type, 
                :tranmission_communication_channelable_id,
                :receiver_communication_channelable_id
  actions :all

  index do
    selectable_column
    id_column
    
    column :name
    column :communication_channel_type
    column :tranmission_communication_channelable
    column :receiver_communication_channelable
  end

  show do |icfn_node|
    attributes_table do
      row :name
      row :communication_channel_type
      row :tranmission_communication_channelable
      row :receiver_communication_channelable
    end
  end

  form do |f|
    f.inputs 'Communication Channel Details' do
      f.semantic_errors *f.object.errors.keys

      selected_communication_channel_type = f.object.new_record? ? CommunicationChannel::COMMUNICATION_CHANNEL_TYPE_HOUSE_TO_ICFN : f.object.communication_channel_type
      f.input :communication_channel_type, as: :select, collection: CommunicationChannel::COMMUNICATION_CHANNEL_TYPES, selected: selected_communication_channel_type
      
      f.input :tranmission_communication_channelable_id
      f.input :receiver_communication_channelable_id
      
      f.actions
    end
  end
end
