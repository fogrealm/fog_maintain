ActiveAdmin.register PlatformError do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  
  index do
    selectable_column
    id_column
    column :name
    column :platform_error_type
    column :reported_at
    column :platform_errorable
  end

  show do |house|
    attributes_table do
      row :name
      row :platform_error_type
      row :reported_at
      row :platform_errorable
    end  
  end

end
