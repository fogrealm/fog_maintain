ActiveAdmin.register HouseEvent do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  permit_params :id, :name, :event_level, :house_event_type, 
                :house_id, :house_section_name, :event_message, 
                :reported_at

  actions :show, :index

  filter :house
  filter :name
  filter :reported_at

  index do
    selectable_column
    id_column
    column :house
    column :house_section
    column :name
    column :reported_at

    actions
  end

  show do |infra_parameter|
    attributes_table do
      row :house
      row :house_section
      row :name
      row :event_level
      row :house_event_type
      row :event_message
      row :reported_at
    end
  end


end
