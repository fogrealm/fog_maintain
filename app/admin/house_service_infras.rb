ActiveAdmin.register HouseServiceInfra do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  permit_params :house_id, :service_infra_id

  actions :all

  filter :house
  filter :service_infra

  index do
    selectable_column
    id_column
    column :house
    column :service_infra

    actions
  end

  show do |end_device|
    attributes_table do
     row :house
     row :service_infra
    end
  end

  form do |f|
    f.inputs 'End Device Details' do
      f.semantic_errors *f.object.errors.keys

      f.input :house_id
      
      selected_service_infra = f.object.new_record? ? ServiceInfra.try(:first) : f.object.service_infra
      f.input :service_infra, as: :select, collection: ServiceInfra.all, selected: selected_service_infra.try(:id)

      f.actions
    end
  end


end
