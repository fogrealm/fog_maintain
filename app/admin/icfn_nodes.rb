ActiveAdmin.register IcfnNode do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  permit_params :name, :base_url, :running_status, :is_recently_updated,
                location_attributes: [ :id, :latitude, :longitude ],
                security_template_attributes: [:id, :symmetric_encryption_scheme]

  actions :all

  index do
    selectable_column
    id_column
    column :name
    column :base_url
    column :is_recently_updated
  end

  show do |icfn_node|
    attributes_table do
      row :name
      row :base_url
      row :is_recently_updated
    end

    panel 'Location' do
      attributes_table_for icfn_node.location do
        row :longitude
        row :latitude
      end
    end

    panel 'Security Template' do
      attributes_table_for icfn_node.security_template do
        row :symmetric_encryption_scheme
      end
    end
  end

  form do |f|
    f.inputs 'ICFN Node Details' do
      f.semantic_errors *f.object.errors.keys

      f.input :name
      f.input :base_url
      f.input :is_recently_updated

      f.inputs "Location Details", for: [ :location, f.object.location || f.object.build_location ] do |location_form|
        location_form.input :latitude
        location_form.input :longitude
      end

      f.inputs "Security Details", for: [ :security_template, f.object.security_template || f.object.build_security_template ] do |security_template_form|
        selected_symmetric_encryption_scheme = security_template_form.object.new_record? ? SecurityTemplate::DEFAULT_SYMMETRIC_ENCRYPTION_SCHEME : security_template_form.object.symmetric_encryption_scheme
        security_template_form.input :symmetric_encryption_scheme, as: :select, collection: OpenSSL::Cipher.ciphers, selected: selected_symmetric_encryption_scheme
      end

      f.actions
    end
  end

  action_item :download_private_key, :only => :show do
    link_to 'Download Private Key', download_private_key_admin_icfn_node_path(icfn_node), method: :get
  end

  member_action :download_private_key, method: :get do
    send_data Base64.decode64(resource.security_template.private_key), :filename => "icfn_node_#{resource.id}_private_key.pem"
  end

end
